# Alright, this is a hell of a bad script. I would LOVE pull requests to 
# improve it, as I am not good at all at programming. But it suites my needs,
# and I hope it suites someone elses too.

# Create a quick date formatter, so that we can datestamp files, so I can
#  compare over time.
NOW=$(date +"%F")

# Alright, here is where I define the repositories for all of the repos I want
# to pull issues for. To grab the URL slugs here, I use the other file in this
# repository, "Get_All_Repos.sh". Check that out if you haven't already, and 
# you should use that to help build this file.

# I have 87, you may have more, or less. I define them all up here, and then
# refer to them often below. This script is currently a bear since most of the
# work I do, I repeat for each of the 87 repos. This could be scripted up much,
# much, much better I'm sure. Any improvement pull requests would be much
# appreciated. For purpose of example below, I've cut down to 50 repos. It  
# works great at my 87, and takes about 30 seconds to run currently.

# An example slug for me here would be "osx-2013-nectar_2014-10-21", which is 
# the repository we have for our OS X application, Nectar. This will be the 
# name of your repository, as pulled from in Get_All_Repos.sh.

LINE1=project-slug-1
LINE2=project-slug-2
LINE3=project-slug-3
LINE4=project-slug-4
LINE5=project-slug-5
LINE6=project-slug-6
LINE7=project-slug-7
LINE8=project-slug-8
LINE9=project-slug-9
LINE10=project-slug-10
LINE11=project-slug-11
LINE12=project-slug-12
LINE13=project-slug-13
LINE14=project-slug-14
LINE15=project-slug-15
LINE16=project-slug-16
LINE17=project-slug-17
LINE18=project-slug-18
LINE19=project-slug-19
LINE20=project-slug-20
LINE21=project-slug-21
LINE22=project-slug-22
LINE23=project-slug-23
LINE24=project-slug-24
LINE25=project-slug-25
LINE26=project-slug-26
LINE27=project-slug-27
LINE28=project-slug-28
LINE29=project-slug-29
LINE30=project-slug-30
LINE31=project-slug-31
LINE32=project-slug-32
LINE33=project-slug-33
LINE34=project-slug-34
LINE35=project-slug-35
LINE36=project-slug-36
LINE37=project-slug-37
LINE38=project-slug-38
LINE39=project-slug-39
LINE40=project-slug-40
LINE41=project-slug-41
LINE42=project-slug-42
LINE43=project-slug-43
LINE44=project-slug-44
LINE45=project-slug-45
LINE46=project-slug-46
LINE47=project-slug-47
LINE48=project-slug-48
LINE49=project-slug-49
LINE50=project-slug-50

# I run this same curl request, basically 50 times here, replacing the repo
# name each time with $LINE1, $LINE2, etc. PLEASE, SOMEONE IMPROVE THIS. I've
# used 50 repositorys/requests here for example. In my personal use, I have 87
# repositories currently which I run through, and this runs great and in about
# ~30 seconds. Your experience may vary here.

# 9magnets is our company name, which is the owner of all of the repos we use.
# For your repos, replace this with the owner of the repos you are grabbing.

# Basic HTTP auth, I use Paw (http://luckymarmot.com/paw) to generate. Use
# whatever you like best.

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE1"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE1\_$NOW.json;
# As you can see, I use ~/Development/Issues as the directory for all of my
# json/markdown. Feel free to choose your own adventure here.

# We've just created a JSON file that contains all of the issues (if any exist
# for the repo), now let's pretty print it using python.
cat ~/Development/Issues/$LINE1\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE1\_"$NOW"_pretty.json;

# We've grabbd all we need for the first repo, let's move along to the second,
# and so forth, for all 87 repos I currently have.
curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE2"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE2\_$NOW.json;

cat ~/Development/Issues/$LINE2\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE2\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE3"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE3\_$NOW.json;

cat ~/Development/Issues/$LINE3\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE3\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE4"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE4\_$NOW.json;

cat ~/Development/Issues/$LINE4\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE4\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE5"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE5\_$NOW.json;

cat ~/Development/Issues/$LINE5\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE5\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE6"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE6\_$NOW.json;

cat ~/Development/Issues/$LINE6\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE6\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE7"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE7\_$NOW.json;

cat ~/Development/Issues/$LINE7\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE7\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE8"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE8\_$NOW.json;

cat ~/Development/Issues/$LINE8\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE8\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE9"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE9\_$NOW.json;

cat ~/Development/Issues/$LINE9\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE9\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE10"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE10\_$NOW.json;

cat ~/Development/Issues/$LINE10\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE10\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE11"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE11\_$NOW.json;

cat ~/Development/Issues/$LINE11\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE11\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE12"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE12\_$NOW.json;

cat ~/Development/Issues/$LINE12\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE12\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE13"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE13\_$NOW.json;

cat ~/Development/Issues/$LINE13\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE13\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE14"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE14\_$NOW.json;

cat ~/Development/Issues/$LINE14\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE14\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE15"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE15\_$NOW.json;

cat ~/Development/Issues/$LINE15\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE15\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE16"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE16\_$NOW.json;

cat ~/Development/Issues/$LINE16\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE16\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE17"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE17\_$NOW.json;

cat ~/Development/Issues/$LINE17\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE17\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE18"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE18\_$NOW.json;

cat ~/Development/Issues/$LINE18\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE18\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE19"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE19\_$NOW.json;

cat ~/Development/Issues/$LINE19\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE19\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE20"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE20\_$NOW.json;

cat ~/Development/Issues/$LINE20\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE20\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE21"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE21\_$NOW.json;

cat ~/Development/Issues/$LINE21\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE21\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE22"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE22\_$NOW.json;

cat ~/Development/Issues/$LINE22\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE22\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE23"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE23\_$NOW.json;

cat ~/Development/Issues/$LINE23\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE23\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE24"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE24\_$NOW.json;

cat ~/Development/Issues/$LINE24\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE24\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE25"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE25\_$NOW.json;

cat ~/Development/Issues/$LINE25\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE25\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE26"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE26\_$NOW.json;

cat ~/Development/Issues/$LINE26\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE26\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE27"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE27\_$NOW.json;

cat ~/Development/Issues/$LINE27\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE27\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE28"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE28\_$NOW.json;

cat ~/Development/Issues/$LINE28\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE28\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE29"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE29\_$NOW.json;

cat ~/Development/Issues/$LINE29\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE29\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE30"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE30\_$NOW.json;

cat ~/Development/Issues/$LINE30\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE30\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE31"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE31\_$NOW.json;

cat ~/Development/Issues/$LINE31\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE31\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE32"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE32\_$NOW.json;

cat ~/Development/Issues/$LINE32\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE32\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE33"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE33\_$NOW.json;

cat ~/Development/Issues/$LINE33\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE33\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE34"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE34\_$NOW.json;

cat ~/Development/Issues/$LINE34\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE34\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE35"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE35\_$NOW.json;

cat ~/Development/Issues/$LINE35\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE35\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE36"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE36\_$NOW.json;

cat ~/Development/Issues/$LINE36\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE36\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE37"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE37\_$NOW.json;

cat ~/Development/Issues/$LINE37\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE37\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE38"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE38\_$NOW.json;

cat ~/Development/Issues/$LINE38\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE38\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE39"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE39\_$NOW.json;

cat ~/Development/Issues/$LINE39\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE39\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE40"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE40\_$NOW.json;

cat ~/Development/Issues/$LINE40\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE40\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE41"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE41\_$NOW.json;

cat ~/Development/Issues/$LINE41\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE41\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE42"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE42\_$NOW.json;

cat ~/Development/Issues/$LINE42\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE42\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE43"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE43\_$NOW.json;

cat ~/Development/Issues/$LINE43\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE43\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE44"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE44\_$NOW.json;

cat ~/Development/Issues/$LINE44\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE44\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE45"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE45\_$NOW.json;

cat ~/Development/Issues/$LINE45\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE45\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE46"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE46\_$NOW.json;

cat ~/Development/Issues/$LINE46\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE46\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE47"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE47\_$NOW.json;

cat ~/Development/Issues/$LINE47\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE47\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE48"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE48\_$NOW.json;

cat ~/Development/Issues/$LINE48\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE48\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE49"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE49\_$NOW.json;

cat ~/Development/Issues/$LINE49\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE49\_"$NOW"_pretty.json;

curl -X GET "https://bitbucket.org/api/1.0/repositories/9magnets/"$LINE50"/issues?status=new&status=open" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/$LINE50\_$NOW.json;

cat ~/Development/Issues/$LINE50\_"$NOW".json | python -m json.tool | cat >> ~/Development/Issues/$LINE50\_"$NOW"_pretty.json;

# PHEW, we're done. Alright, now we're going to run sed to remove a TON of 
# stuff from the JSON that I personally don't use. You may want to keep some of
# this, and if so, remove it. It's just not of value for me, so I chop it.

find ~/Development/Issues -type f -exec sed -i.bak '/follower_count/d' {} \;
# I always remove these pesky .bak files, because they just group up and make
# Finder unusable. Probably a better way to do this.
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/comment_count/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/local_id/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/is_spam/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/first_name/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/last_name/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/username/d' {} \;
rm -f ~/Development/Issues/*.bak;
# The API endpoint resource URL isn't something you could copy/paste into a
# browser, so I fix that right here.
find ~/Development/Issues -type f -exec sed -i.bak 's/\"resource_uri\": \"\/1.0\/repositories/\"url\": \"https:\/\/bitbucket.org/' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\/1.0\/users/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/avatar/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/is_staff/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/is_team/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/status/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/utc_created_on/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/null/d' {} \;
rm -f ~/Development/Issues/*.bak;
# Delete a lot of wasted lines.
find ~/Development/Issues -type f -exec sed -i.bak '1,14d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/^$/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/./!d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak 's/\r\n/-/g' {} \;
rm -f ~/Development/Issues/*.bak;
# I substitute the UTC last updated time with a bit of markdown that creates
# "New Issue" headers for each individual issue, so I can read it easily 
# scrolling through.
find ~/Development/Issues -type f -exec sed -i.bak 's/.*utc_last_updated.*/\
***NEW ISSUE***/' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/},/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/metadata/d' {} \;
rm -f ~/Development/Issues/*.bak;
# Bitbucket's API pulls down URL resources pointing at /issues/, but if you
# link to this, you'll get a 404. So I replace with /issue/, so I can copy/
# paste into my browser.
find ~/Development/Issues -type f -exec sed -i.bak 's/\/issues\//\/issue\//' {} \;
rm -f ~/Development/Issues/*.bak;

# Here, I flip around the contents of each document so that when scrolling top
# to bottom, the issue name appears above the description/content, and turn the
# pretty printed json file into a more appropriately named markdown file (so I
# can load it into Marked if I want). Again, there has to be a better way to do
# this, but I'm bad at bash scripting and programming in general, and 
# especially the cat commmand.
tail -r ~/Development/Issues/$LINE1\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE1\_"$NOW".md
tail -r ~/Development/Issues/$LINE2\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE2\_"$NOW".md
tail -r ~/Development/Issues/$LINE3\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE3\_"$NOW".md
tail -r ~/Development/Issues/$LINE4\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE4\_"$NOW".md
tail -r ~/Development/Issues/$LINE5\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE5\_"$NOW".md
tail -r ~/Development/Issues/$LINE6\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE6\_"$NOW".md
tail -r ~/Development/Issues/$LINE7\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE7\_"$NOW".md
tail -r ~/Development/Issues/$LINE8\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE8\_"$NOW".md
tail -r ~/Development/Issues/$LINE9\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE9\_"$NOW".md
tail -r ~/Development/Issues/$LINE10\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE10\_"$NOW".md
tail -r ~/Development/Issues/$LINE11\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE11\_"$NOW".md
tail -r ~/Development/Issues/$LINE12\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE12\_"$NOW".md
tail -r ~/Development/Issues/$LINE13\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE13\_"$NOW".md
tail -r ~/Development/Issues/$LINE14\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE14\_"$NOW".md
tail -r ~/Development/Issues/$LINE15\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE15\_"$NOW".md
tail -r ~/Development/Issues/$LINE16\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE16\_"$NOW".md
tail -r ~/Development/Issues/$LINE17\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE17\_"$NOW".md
tail -r ~/Development/Issues/$LINE18\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE18\_"$NOW".md
tail -r ~/Development/Issues/$LINE19\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE19\_"$NOW".md
tail -r ~/Development/Issues/$LINE20\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE20\_"$NOW".md
tail -r ~/Development/Issues/$LINE21\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE21\_"$NOW".md
tail -r ~/Development/Issues/$LINE22\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE22\_"$NOW".md
tail -r ~/Development/Issues/$LINE23\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE23\_"$NOW".md
tail -r ~/Development/Issues/$LINE24\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE24\_"$NOW".md
tail -r ~/Development/Issues/$LINE25\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE25\_"$NOW".md
tail -r ~/Development/Issues/$LINE26\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE26\_"$NOW".md
tail -r ~/Development/Issues/$LINE27\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE27\_"$NOW".md
tail -r ~/Development/Issues/$LINE28\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE28\_"$NOW".md
tail -r ~/Development/Issues/$LINE29\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE29\_"$NOW".md
tail -r ~/Development/Issues/$LINE30\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE30\_"$NOW".md
tail -r ~/Development/Issues/$LINE31\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE31\_"$NOW".md
tail -r ~/Development/Issues/$LINE32\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE32\_"$NOW".md
tail -r ~/Development/Issues/$LINE33\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE33\_"$NOW".md
tail -r ~/Development/Issues/$LINE34\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE34\_"$NOW".md
tail -r ~/Development/Issues/$LINE35\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE35\_"$NOW".md
tail -r ~/Development/Issues/$LINE36\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE36\_"$NOW".md
tail -r ~/Development/Issues/$LINE37\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE37\_"$NOW".md
tail -r ~/Development/Issues/$LINE38\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE38\_"$NOW".md
tail -r ~/Development/Issues/$LINE39\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE39\_"$NOW".md
tail -r ~/Development/Issues/$LINE40\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE40\_"$NOW".md
tail -r ~/Development/Issues/$LINE41\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE41\_"$NOW".md
tail -r ~/Development/Issues/$LINE42\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE42\_"$NOW".md
tail -r ~/Development/Issues/$LINE43\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE43\_"$NOW".md
tail -r ~/Development/Issues/$LINE44\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE44\_"$NOW".md
tail -r ~/Development/Issues/$LINE45\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE45\_"$NOW".md
tail -r ~/Development/Issues/$LINE46\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE46\_"$NOW".md
tail -r ~/Development/Issues/$LINE47\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE47\_"$NOW".md
tail -r ~/Development/Issues/$LINE48\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE48\_"$NOW".md
tail -r ~/Development/Issues/$LINE49\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE49\_"$NOW".md
tail -r ~/Development/Issues/$LINE50\_"$NOW"_pretty.json | cat > ~/Development/Issues/$LINE50\_"$NOW".md

#Alright, now I remove all very small sized files (which contain 0 issues,)
find ~/Development/Issues/ -type f -size -2 -exec rm -f {} \;

# Delete excess space.
find ~/Development/Issues -type f -exec sed -i.bak '1,3d' {} \;
rm -f ~/Development/Issues/*.bak;

# A markdown legible title for each document.
sed -i.bak '1s/^/### '$LINE1'\
\
/' ~/Development/Issues/$LINE1\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE2'\
\
/' ~/Development/Issues/$LINE2\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE3'\
\
/' ~/Development/Issues/$LINE3\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE4'\
\
/' ~/Development/Issues/$LINE4\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE5'\
\
/' ~/Development/Issues/$LINE5\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE6'\
\
/' ~/Development/Issues/$LINE6\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE7'\
\
/' ~/Development/Issues/$LINE7\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE8'\
\
/' ~/Development/Issues/$LINE8\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE9'\
\
/' ~/Development/Issues/$LINE9\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE10'\
\
/' ~/Development/Issues/$LINE10\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE11'\
\
/' ~/Development/Issues/$LINE11\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE12'\
\
/' ~/Development/Issues/$LINE12\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE13'\
\
/' ~/Development/Issues/$LINE13\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE14'\
\
/' ~/Development/Issues/$LINE14\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE15'\
\
/' ~/Development/Issues/$LINE15\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE16'\
\
/' ~/Development/Issues/$LINE16\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE17'\
\
/' ~/Development/Issues/$LINE17\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE18'\
\
/' ~/Development/Issues/$LINE18\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE19'\
\
/' ~/Development/Issues/$LINE19\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE20'\
\
/' ~/Development/Issues/$LINE20\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE21'\
\
/' ~/Development/Issues/$LINE21\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE22'\
\
/' ~/Development/Issues/$LINE22\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE23'\
\
/' ~/Development/Issues/$LINE23\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE24'\
\
/' ~/Development/Issues/$LINE24\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE25'\
\
/' ~/Development/Issues/$LINE25\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE26'\
\
/' ~/Development/Issues/$LINE26\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE27'\
\
/' ~/Development/Issues/$LINE27\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE28'\
\
/' ~/Development/Issues/$LINE28\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE29'\
\
/' ~/Development/Issues/$LINE29\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE30'\
\
/' ~/Development/Issues/$LINE30\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE31'\
\
/' ~/Development/Issues/$LINE31\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE32'\
\
/' ~/Development/Issues/$LINE32\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE33'\
\
/' ~/Development/Issues/$LINE33\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE34'\
\
/' ~/Development/Issues/$LINE34\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE35'\
\
/' ~/Development/Issues/$LINE35\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE36'\
\
/' ~/Development/Issues/$LINE36\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE37'\
\
/' ~/Development/Issues/$LINE37\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE38'\
\
/' ~/Development/Issues/$LINE38\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE39'\
\
/' ~/Development/Issues/$LINE39\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE40'\
\
/' ~/Development/Issues/$LINE40\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE41'\
\
/' ~/Development/Issues/$LINE41\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE42'\
\
/' ~/Development/Issues/$LINE42\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE43'\
\
/' ~/Development/Issues/$LINE43\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE44'\
\
/' ~/Development/Issues/$LINE44\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE45'\
\
/' ~/Development/Issues/$LINE45\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE46'\
\
/' ~/Development/Issues/$LINE46\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE47'\
\
/' ~/Development/Issues/$LINE47\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE48'\
\
/' ~/Development/Issues/$LINE48\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE49'\
\
/' ~/Development/Issues/$LINE49\_"$NOW".md;

sed -i.bak '1s/^/### '$LINE50'\
\
/' ~/Development/Issues/$LINE50\_"$NOW".md;

# Remove all of the old JSON files, and the pesky .bak files.
rm -rf ~/Development/Issues/*.json
rm -f ~/Development/Issues/*.bak;

# run another deletion of small files, in case any were created in the naming
# above.
find ~/Development/Issues/ -type f -size -2 -exec rm -f {} \;

# I add a small markdown formatted END OF REPO, so when scrolling, I can
# quickly see where one repository ends and another starts.
for file in ~/Development/Issues/*.md; do
  echo "###### END OF REPO

" >> "$file"
done

# FINALLY, I concatenate all of these markdown fils into a single long file
# that I can scroll through easily all at once.
cat ~/Development/Issues/*.md > ~/Development/Issues/Issue_Report_"$NOW".md

# Clean up any leftover garbage.
rm -rf *.md.*
rm -rf ~/Development/Issues/.DS_Store.*
rm -rf ~/Development/Issues/.*.DS_Store.*
rm -rf ~/Development/Issues/.*.DS_Store

# Alright, should have a ton of issues, grouped by repository, that look like
# this. One small note, the responsible and reported_by user descriptors do
# appear under the appropriate user's name, because of the tail command. I
# should find a way to fix this, but it exists for now.

# ***NEW ISSUE***
#             "title": "Example Title",
#                 "display_name": "Responsible User",
#             "responsible": {
#             "url": "https://bitbucket.org/9magnets/project/issue/40",
#                 "display_name": "Reported User",
#             "reported_by": {
#             "priority": "major",
#                 "kind": "bug",
#             "created_on": "2013-08-17T22:13:52.798",
#             "content": "Here's the content of the issue. Note, I don't keep any issue followup comments, etc. I go to URL for that.",
#         {

# I personally find this much quicker and easier to manage than a ton of URLs
# all in different repos. Hope you find it useful and enjoy it!