# Pull all repos and cut down to Slug/URL. This is a really big mess/brute
# force kind of way, but it works for me.

# I'm not good with sed at all, and find that if I don't run this rm, I get a
# ton of DS_Store files that cause Finder performance issues, so let's clean up
# quick.
rm -rf ~/Development/Issues/.DS_Store.*
rm -rf ~/Development/Issues/.*.DS_Store.*
rm -rf ~/Development/Issues/.*.DS_Store

# Create a quick date formatter, so that we can datestamp files, so I can
# compare over time.
NOW=$(date +"%F")

# Alright, here I set up the curl request to pull all of the repos I follow on
# Bitbucket. I used Paw (http://luckymarmot.com/paw) to grab the Authorization
# for my account. However, do this however you feel comfortable, and replace
# BASIC_HTTP_AUTH_CREDENTIALS with your credentials.

curl -X GET "https://bitbucket.org/api/1.0/user/repositories/dashboard/" \
     -H "Authorization: BASIC_HTTP_AUTH_CREDENTIALS" \
     -m 30 \
     -v \
| cat >> ~/Development/Issues/current_repository_list_$NOW.json

# As obvious above and below, ~/Development/Issues/ is the directory I'm using
# for this on my laptop. Feel free to replace/modify. I place all of my git
# repos in ~/Development, so a separate directory inside for issues makes sense
# for me. Maybe not for you though?

# Use Python's JSON tool to pretty print this file into something readable and
# manage able. 
cat ~/Development/Issues/current_repository_list_$NOW.json | python -m json.tool | cat >> ~/Development/Issues/current_repository_list_pretty_$NOW.md
rm -f ~/Development/Issues/current_repository_list_$NOW.json

# I then use sed to remove any lines that are irrelevant to me. You may want
# some of these, you may want to remove more than I did. Feel free to play
# around.
find ~/Development/Issues -type f -exec sed -i.bak '/\"avatar\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"first_name\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"last_name\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"is_staff\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"is_team\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"resource_uri\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"website\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/\"fork\"/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/scm/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/is_private/d' {} \;
rm -f ~/Development/Issues/*.bak;
find ~/Development/Issues -type f -exec sed -i.bak '/_pk/d' {} \;
rm -f ~/Development/Issues/*.bak;

# I'm not good with sed at all, and find that if I don't run this rm, I get a
# ton of DS_Store files that cause Finder performance issues, so let's clean up
# quick.
rm -rf ~/Development/Issues/.DS_Store.*
rm -rf ~/Development/Issues/.*.DS_Store.*
rm -rf ~/Development/Issues/.*.DS_Store

# After I do this, I have a markdown file in my Issues directory which lists
# out all of the repos I am following for work projects on Bitbucket (currently
# at 87 private repos we have for work. We do A LOT of contract projects.) The
# output looks something like this:
# {
#                 "absolute_url": "/cbanga/bash-20141021-pull_all_issues/",
#                 "name": "bash-20141021-pull_all_issues",
#                 "owner": "cbanga",
#                 "slug": "bash-20141021-pull_all_issues",
#             }
# With this info, I use the absolute URL and then move over to the other
# script in this repo, Get_All_Open_Issues.sh.