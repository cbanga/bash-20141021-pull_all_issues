# Pull All Bitbucket Project Issues #

##### A couple, simple bash scripts that help me pull down and see all issues currently open on our private Bitbucket repos for work.

Hopefully, this will be of use to somebody. Inside are two small scripts that allow me to create a single markdown file with the contents of every open issue on our team's Bitbucket account. The first script, Get\_All\_Repos.sh will help you pull down the URL slugs of all repos currently followed by your user account.

The second, Get\_All\_Open\_Issues.sh will allow you to take those slugs and then create a single markdown file, full of the contents of every single issue in a Bitbucket repository you follow. Makes it great for copying/pasting blurbs into emails for team members, general keep up with a hundred or so issues at once, etc. Here's an example of what the final markdown file contains as an issue. My personal file will contain between 90-150 issues at any given time, through my 87 repos currently.:

		***NEW ISSUE***
             "title": "Example Title",
                 "display_name": "Responsible User",
             "responsible": {
             "url": "https://bitbucket.org/9magnets/project/issue/40",
                 "display_name": "Reported User",
             "reported_by": {
             "priority": "major",
                 "kind": "bug",
             "created_on": "2013-08-17T22:13:52.798",
             "content": "Issue content. Note, I don't keep followup comments. Head to the URL for that.",
         {

It's probably not useful for most, but it's useful for me, so hopefully someone can make use of or improve these scripts.